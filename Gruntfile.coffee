module.exports = (grunt) ->
        
        require('load-grunt-tasks')(grunt)
        require('time-grunt')(grunt)

        grunt.initConfig
                
                watch:
                        dev:
                                files: ['**/*.html', '**/*.css']
                                options:
                                        livereload: yes

                connect:
                        dev:
                                options:
                                        port: 9001,
                                        livereload: yes

        grunt.registerTask 'dev', [
                'connect:dev'
                'watch:dev'
        ]
