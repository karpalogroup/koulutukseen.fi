jQuery(document).ready(function($) {
   var li = $('.menukoulutusvalikko > .deeper');
   var link = li.find('> a');

   link.on('click', function(e) {
      e.preventDefault();
      li.toggleClass('submenu-active');
   });
}
);
