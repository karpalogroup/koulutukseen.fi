# koulutukseen.fi

Sisältää sivuston sivupohjan tyylit, skriptit ja HTML-rakenteen mallipohjat

* Päätyylitiedosto `styles/template.css`
* Skriptitiedosto `scripts/custom.js`
